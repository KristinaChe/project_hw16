
#include <iostream>
#include <ctime>

int main()
{
    const int N = 21;
    int array[N][N];

    //matrix filling 
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            array[i][j] = i + j;
        }
    }

    //matrix output
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            std::cout << array[i][j] << " ";
        }
        std::cout << '\n';
    }

    int sum = 0;
    time_t now = time(0);
    struct tm timeinfo;
    localtime_s(&timeinfo, &now);
    int m = (timeinfo.tm_mday) % N; //finding the index of an array string

    for (int x = 0; x < N; x++)
    {
        sum += array[m][x];
    }
   
    std::cout << sum << '\n';
    
    return 0;
}
